const fs = require("fs").promises;

/**
 * Here is an example of the FileDatabase 
 * The whole file represnts a datbase that can contain multiple collections 
 * where each collection is represnted by its name as a key in the database object 
 * E.g: 
 * {
 *    events : {
 *      lastIncrementalId: 2, 
 *      documents: [ 
 *          {
 *             id : 1 , 
 *             name: "Not Very Important Event"
 *             date : "2021-11-17T11:28:08.659Z"
 *           }    
 *      ]
 *    }
 * }
 */
class FileDatabase {

    constructor(connectionString) {
        this.filePath = connectionString;
    }


    async load() {

        try {
            return JSON.parse(await fs.readFile(this.filePath))
        } catch (err) {
            // if file not found return an empty object ( first query )
            if (err.code == 'ENOENT')
                return {}
            throw err;
        }


    }

    async save(obj) {
        return fs.writeFile(this.filePath, JSON.stringify(obj))
    }

    emptyCollection() {
        return { lastIncrementalId: 1, documents: [] };
    }

    async loadCollection(collectionKey) {
        const db = await this.load();
        return typeof db[collectionKey] == 'undefined' ? this.emptyCollection() : db[collectionKey];
    }

    async saveCollection(collectionKey, collection) {
        const obj = await this.load();
        obj[collectionKey] = collection;
        return this.save(obj);
    }

}

module.exports = function (connectionString) {
    return new FileDatabase(connectionString);
}