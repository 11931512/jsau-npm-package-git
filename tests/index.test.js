let databaseFactory = require("../index")
describe("Naive Filedatabase Test", () => {
    let db;
    let data = {
        "event": {
            "lastIncrementalId": 16,
            "documents": [
                {
                    "name": "Very Important Event ",
                    "date": "2021-11-12",
                    "id": 2
                }]
        }
    };
    beforeEach(() => {
        db = databaseFactory("db.test.json");
    })

    it("LOAD EMPTY DATA", async () => {
        await expect(db.load()).resolves.toEqual({})
    });
    it("SAVE DATA", async () => {
        await expect(db.save(data)).resolves.toBe();
    });
    it("LOAD SAVED DATA", async () => {
        await expect(db.load()).resolves.toEqual(data);
    });

    it("LOAD COLLECTION", async () => {
        await expect(db.loadCollection("event")).resolves.toEqual(data["event"]);
    });

})